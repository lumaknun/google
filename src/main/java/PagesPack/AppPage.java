package PagesPack;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import LocatorPack.AppLock;


public class AppPage extends AppLock {
	WebDriver driver;
	
	public AppPage(WebDriver driver){
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	public void searchOnGoogle(String text){
		returnGoogleSearch().sendKeys(text);
	}

}
