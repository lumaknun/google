package UtilPack;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class AppBase {
public WebDriver driver;
	
	@BeforeMethod(alwaysRun=true)
	public void startBrowser(){
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"/driver/chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("https://www.google.com");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        
	}
	
	@AfterMethod(alwaysRun=true)
	public void closeBrowser(ITestResult result){
		if(ITestResult.FAILURE == result.getStatus()){
	        
            File src= ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
            try {
                FileUtils.copyFile(src, new File(System.getProperty("user.dir")+"/Screenshots/"+result.getName()+".png"));
            }
           
            catch (IOException e)
            {
              System.out.println(e.getMessage());
           
            }
       }
	driver.close();     
	}

}
